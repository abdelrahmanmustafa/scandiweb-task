<?php
namespace utl;

use models;

class ProductFactory
{

    public static function create_product($product)
    {

        $factory = [
            'dvd' => new models\Dvd($product),
            'furniture' => new models\Furniture($product),
            'book' => new models\Book($product),
        ];
        return $factory[$product->type];
    }
}

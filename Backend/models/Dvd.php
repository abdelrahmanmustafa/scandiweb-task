<?php
namespace models;

class Dvd extends Product
{
    private $table = 'dvd';

    public $size;

    public function __construct($product)
    {

        $this->sku = $product->sku;
        $this->name = $product->name;
        $this->price = $product->price;
        $this->size = $product->size;

    }

    public function insert($db): string
    {
        $this->conn = $db;

        // the query
        $query = 'INSERT INTO product SET sku = :sku, name = :name, price = :price ,type = "dvd" ;
                    INSERT INTO ' . $this->table . ' SET dvd_sku = :sku, size = :size ;';

        // statement
        $stmt = $this->conn->prepare($query);

        // Bind data
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':size', $this->size);

        // Execute query
        if ($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: %s.\n", $stmt->error);

        return false;
    }
}

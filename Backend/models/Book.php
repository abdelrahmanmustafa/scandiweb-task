<?php
namespace models;

class Book extends Product
{
    private $table = 'book';

    public $weight;

    public function __construct($product)
    {

        $this->sku = $product->sku;
        $this->name = $product->name;
        $this->price = $product->price;
        $this->weight = $product->weight;
        $this->type = 'book';

    }

    public function insert($db): string
    {
        $this->conn = $db;
        // the query
        $query = 'INSERT INTO product SET sku = :sku, name = :name, price = :price, type ="book";
                    INSERT INTO ' . $this->table . ' SET book_sku = :sku, weight = :weight ;';

        // statement
        $stmt = $this->conn->prepare($query);

        // Bind data
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':weight', $this->weight);

        // Execute query
        if ($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: %s.\n", $stmt->error);

        return false;
    }
}

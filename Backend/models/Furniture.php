<?php
namespace models;

class Furniture extends Product
{
    private $table = 'furniture';

    public $height;
    public $width;
    public $length;

    public function __construct($product)
    {

        $this->sku = $product->sku;
        $this->name = $product->name;
        $this->price = $product->price;
        $this->height = $product->height;
        $this->width = $product->width;
        $this->length = $product->length;

    }

    public function insert($db): string
    {
        $this->conn = $db;
        // the query
        $query = 'INSERT INTO product SET sku = :sku, name = :name, price = :price, type = "furniture";
                    INSERT INTO ' . $this->table . ' SET furniture_sku = :sku, length = :length,
                     width = :width, height = :height ;';

        // statement

        $stmt = $this->conn->prepare($query);

        // Bind data
        $stmt->bindParam(':sku', $this->sku);
        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':price', $this->price);
        $stmt->bindParam(':height', $this->height);
        $stmt->bindParam(':width', $this->width);
        $stmt->bindParam(':length', $this->length);

        // Execute query
        if ($stmt->execute()) {
            return true;
        }

        // Print error if something goes wrong
        printf("Error: %s.\n", $stmt->error);

        return false;
    }
}

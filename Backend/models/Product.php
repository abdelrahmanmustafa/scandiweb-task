<?php
namespace models;

abstract class Product
{

    protected $conn;
    private $table = 'product';


    public $sku;
    public $name;
    public $price;

    abstract public function insert($db): string;

    public static function delete($conn, $sku)
    {
        $query = 'DELETE FROM product WHERE sku = :sku';

        // Prepare statement
        $stmt = $conn->prepare($query);

        // Bind data
        $stmt->bindParam(':sku', $sku);

        // Execute qusery
        if ($stmt->execute()) {
            if($stmt->rowCount()>0){

                return true;
            }
        }
        return false;
    }

    public static function read($conn)
    {

        // Create query
        $query = 'SELECT *
              FROM product p
              LEFT JOIN book b on b.book_sku = p.sku
              LEFT JOIN furniture f on f.furniture_sku = p.sku
              LEFT JOIN dvd d on d.dvd_sku = p.sku
              order by id
              '
        ;

        // Prepare statement
        $stmt = $conn->prepare($query);

        // Execute query
        $stmt->execute();

        return $stmt;
    }

}
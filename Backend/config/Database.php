<?php
namespace config;

use PDO;

class Database
{

    private static $conn;

    private function __construct()
    {}

    public static function connection()
    {
        if (empty(self::$conn)) {
            self::$conn = null;
            $host = "localhost";
            $db_name = "id18670688_app_for_the_task";
            $user = "id18670688_root";
            $password = "r4pzaoZ#H<3Q(Pju";

            try {
                self::$conn = new PDO('mysql:host=' . $host . ';dbname=' . $db_name,
                    $user, $password);
                self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            } catch (\PDOException $e) {
                echo json_encode(
                    array('message' => 'INVALID input')
                );

            }
        }
        return self::$conn;
    }

}

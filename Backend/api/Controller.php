<?php
namespace api;

use config;
use models;
use utl;

class Controller
{

    public static function get_products()
    {
        //Result of getting data from datavase
        $result = models\Product::read(config\Database::connection());
        // Get row count
        $num = $result->rowCount();

        // Check if any product
        if ($num > 0) {

            // Product array
            $product_arr = array();

            while ($row = $result->fetch(\PDO::FETCH_ASSOC)) {

                //USing factory patrn to crat an instans of object
                $product_item = utl\ProductFactory::create_product((object) $row);

                // Push to product_item
                array_push($product_arr, $product_item);
            }

            // Turn to JSON & output
            echo json_encode($product_arr);
        } else {
            // No Products
            echo json_encode(
                array()
            );
        }

    }
    public static function post_products($data)
    {

        //make db connection
        $db = config\Database::connection();

        try{

            // Get an instans as we do in get
            $product = utl\ProductFactory::create_product($data);
            
            //try to insert and if work pass maseg
            if ($product->insert($db)) {
                
                echo json_encode(
                    array('message' => 'Product Created')
                );
            } else {
                echo json_encode(
                    array('message' => 'Product Not Created')
                );
            }
        }catch (\Exception $e){
            echo json_encode(
                array('message' => 'invaled product format')
            );
        }
    }
    public static function delete_products($data)
    {
        //make db connection
        $db = config\Database::connection();
        try{

            $delete = false;
            foreach ($data->sku as $record) {
                if (models\Product::delete($db, $record)) {$deleted = true;} else { $deleted = false;}
            }
            if ($deleted) {
                echo json_encode(
                    array('message' => 'Product Deleted')
                );
            } else {
                echo json_encode(
                    array('message' => 'No such Product')
                );
            }
        }catch (\Exception $e){
            echo json_encode(
                array('message' => 'ERROR IN DELETION')
            );
        }
    }
}

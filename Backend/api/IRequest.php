<?php
namespace api;

interface IRequest
{
    public function getBody();
}

<?php
include_once __DIR__ . '/autoloder.php';

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header("Access-Control-Allow-Methods: GET,POST,DELETE");
header("Access-Control-Allow-Headers: Accept,Content-Type,Cache-Control");

$request = new api\Request;
$router = new api\Router($request);

switch ($request->requestMethod) {

    //000webhosting dosent support DELETE requste in free plan so i make it post requste.
    // but i have  worked on delete in localhost and it works . 
    case "POST":
        if ($request->getBody()->name) {
            $router->post('/', api\Controller::post_products($request->getBody()));
        } elseif (!$request->getBody()->name) {
            $router->delete('/', api\Controller::delete_products($request->getBody()));
        }
        break;
    case "GET":
        $router->get('/', api\Controller::get_products());
        break;
        // case "DELETE":
        //     $router->delete('/', api\Controller::delete_products($request->getBody()));
        //     break;

}

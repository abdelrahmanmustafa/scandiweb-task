import { configureStore } from "@reduxjs/toolkit";
import SKUReducer from "../features/skuSlice";
import updateReducer from "../features/updateSlice"
import productReducer from "../features/productSlice"
export default configureStore({
  reducer: {
    SKUList: SKUReducer,
    updateList: updateReducer,
    saveProduct : productReducer,
  },
});

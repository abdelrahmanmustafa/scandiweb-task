import React from "react";
import Footer from "../../containers/Footer/Footer";
import Header from "../../containers/Header/Header";
import ProductList from "../../containers/ProductList/ProductList";
import classes from "./ProductListPage.module.css";

const ProductListPage = (props) => {
  return (
    <div className={classes.ProductListPage}>
      <Header title = 'Product List' page = "productlist"/>
      <ProductList />
      <Footer />
    </div>
  );
};

export default ProductListPage;

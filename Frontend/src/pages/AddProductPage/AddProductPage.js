import React from "react";
import Header from "../../containers/Header/Header";
import classes from "./AddProductPage.module.css";
import Footer from "../../containers/Footer/Footer";
import ProductForm from "../../containers/ProductForm/ProductForm";


const AddProductPage = (props) => {
  return (
    <div className={classes.AddProductPage}>
      <Header title = "Product Add" page = "add" />
      <ProductForm/>
      <Footer/>
    </div>
  );
};

export default AddProductPage;

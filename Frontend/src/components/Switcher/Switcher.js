import axios from "axios";
import React, { useEffect, useState ,useRef} from "react";
import { useSelector,useDispatch } from "react-redux";
import { setUpdateList } from "../../features/updateSlice";
import classes from "./Switcher.module.css";


const Switcher = (props) => {
  const { saveProduct } = useSelector((state) => state);
    const [value, setValue] = useState("0");
    const sizeRef = useRef();
    const weightRef = useRef();
    const heightRef = useRef();
    const widthRef = useRef();
    const lengthRef = useRef();
    useEffect(() => {
      if (saveProduct) {
        const data ={}
        switch (value) {
          case "1":
            data.size = sizeRef.current.value;
            data.type = 'dvd';

            break;
          
          case "2":
            data.weight = weightRef.current.value;  
            data.type = 'book';

            break;
          
          case "3":
            data.height = heightRef.current.value;
            data.width = widthRef.current.value;
            data.length = lengthRef.current.value;
            data.type = 'furniture';
            break;      
        
          default:
            break;
        }
        props.getSwitcherData(data);
          

  }
      
    },  [saveProduct]);

  return (
    <div className={classes.switcherCont}>
    <select  id ="productType" defaultValue = "0"  className={classes.Switcher} onChange={e => setValue(e.target.value)} > 
    <option value="0" hidden >Choose here</option> 
    <option id = "DVD" value="1"> DVD  </option>
    <option id = "Book" value="2"> Book</option>
    <option id = "Furniture" value="3"> Furniture </option>
    </select>
    { value === "1" ?
        <div>
            <p>Please, provide size</p>
        <label>size(MB):
        <input  id = "size"
          type="text" ref = {sizeRef}
        /></label>
        </div> 
        :value === "2"? 
        <div>
            <p>Please, provide weight</p>
        <label>weight(KG):
        <input  id = "weight"
          type="text" ref = {weightRef}
        /></label>
        </div> 
        :value === "3"?
        <div> 
            <p>Please, provide dimensions</p>
        <label>height(CM):
        <input id = "height"
          type="text" ref = {heightRef}
        />
      </label>
      <label>width(CM):
        <input  id = "width"
          type="text" ref = {widthRef}
        />
      </label>
      <label>length(CM):
        <input  id = "length"
          type="text" ref = {lengthRef}
        />
       </label> </div>
        :""}
    </div>
    

  );
};

export default Switcher;





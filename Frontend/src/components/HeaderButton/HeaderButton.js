import axios from "axios";
import React from "react";
import { useSelector,useDispatch } from "react-redux";
import { setUpdateList } from "../../features/updateSlice";
import { setSaveProduct } from "../../features/productSlice";


import classes from "./HeaderButton.module.css";

const HeaderButton = (props) => {
  const { SKUList } = useSelector((state) => state);
  const dispatch = useDispatch();
  const onClickHandler = () => {
    if (props.type === "delete") {
      const data = {
        sku: SKUList,
      };
      fetch('https://my-scandi-task.000webhostapp.com/scandi_task/', {
            method: 'post',
            
            body: JSON.stringify(data),
          }).then(function(response) {
            dispatch(setUpdateList(true));
            return response.json();
          }); 
   
    }
    else if(props.type ==="save"){
      dispatch(setSaveProduct(true));
   }
   else if(props.type === "cancel"){
    dispatch(setUpdateList(true));
   }
   };

  return (
    <button
      id={`${props.type}-product-btn`}
      className={classes.HeaderButton}
      onClick={onClickHandler}
    >
      <p className={classes.text}>{props.children}</p>
    </button>
  );
};

export default HeaderButton;

import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setSKUList } from "../../features/skuSlice";
import classes from "./ProductCard.module.css";

const ProductCard = (props) => {
  const [checked, setChecked] = useState(false);
  const dispatch = useDispatch();
  const { SKUList } = useSelector((state) => state);

  useEffect(() => {
    if (checked) {
      const idx = SKUList.indexOf(props.SKU);
      if (idx === -1) {
        const tempList = [...SKUList];
        tempList.push(props.SKU);
        dispatch(setSKUList(tempList));
      }
    } else {
      const idx = SKUList.indexOf(props.SKU);
      if (idx > -1) {
        const tempList = [...SKUList];
        tempList.splice(idx, 1);
        dispatch(setSKUList(tempList));
      }
    }
  }, [checked]);

  return (
    <div className={classes.ProductCard}>
      <input
        type="checkbox"
        className={["delete-checkbox", classes.checkbox].join(" ")}
        onClick={() => setChecked(!checked)}
      />
      <p className={classes.text}>{props.SKU}</p>
      <p className={classes.text}>{props.name}</p>
      <p className={classes.text}>{props.price} $</p>
      <p className={classes.text}>
        {props.size
          ? `Size: ${props.size} MB`
          : props.weight
            ? `Weight: ${props.weight}KG`
            : props.height
              ? `Dimensions: ${props.length}x${props.width}x${props.height}`
              : ""}
      </p>
    </div>
  );
};

export default ProductCard;

import { createSlice } from "@reduxjs/toolkit";

export const skuSlice = createSlice({
  name: "SKUs",
  initialState: [],
  reducers: {
    setSKUList: (state, action) => (state = action.payload),
  },
});


export const { setSKUList } = skuSlice.actions;

export default skuSlice.reducer;

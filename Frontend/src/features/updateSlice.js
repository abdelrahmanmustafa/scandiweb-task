import { createSlice } from "@reduxjs/toolkit";

export const updateSlice = createSlice({
  name: "updateList",
  initialState: true,
  reducers: {
    setUpdateList: (state, action) => (state = action.payload),
  },
});


export const { setUpdateList } = updateSlice.actions;

export default updateSlice.reducer;

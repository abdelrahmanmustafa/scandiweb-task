import React from "react";
import HeaderButton from "../../components/HeaderButton/HeaderButton";
import classes from "./Header.module.css";
import { Link } from "react-router-dom";

const Header = (props) => {
  return (
    <div className={classes.Header}>
      <h1 className={classes.title}>{props.title}</h1>
      {props.page === "productlist" ? 
        <div className={classes.btnsContainer}>
        <Link to="/add-product">
          <HeaderButton type="add">ADD</HeaderButton>
        </Link>
        <HeaderButton type="delete">MASS DELETE</HeaderButton>
      </div>  
    :
    <div className={classes.btnsContainer}>
    
    <HeaderButton type="save">Save</HeaderButton>
    
    <Link to="/">
      <HeaderButton type="cancel">Cancel</HeaderButton>
    </Link>
  </div>
    }
    </div>
  );
};

export default Header;

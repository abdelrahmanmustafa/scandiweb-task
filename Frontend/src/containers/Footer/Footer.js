import React from "react";
import classes from "./Footer.module.css";

const Footer = (props) => {
  return (
    <div className={classes.Footer}>
      <p>Scandiweb Test assignment</p>
    </div>
  );
};

export default Footer;

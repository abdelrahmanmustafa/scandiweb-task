import axios from "axios";
import React, { useEffect, useState } from "react";
import ProductCard from "../../components/ProductCard/ProductCard";
import classes from "./ProductList.module.css";
import { useSelector , useDispatch } from "react-redux";
import { setUpdateList } from "../../features/updateSlice";

const ProductList = (props) => {
  const [data, setData] = useState([]);
  
  const { updateList } = useSelector((state) => state);
  const dispatch = useDispatch();

  const responseHandler = (res) => {

    console.log(res.data)
    setData(res.data);
    dispatch(setUpdateList(false));
  };

  useEffect(() => {
    if(updateList){
    axios.get('https://my-scandi-task.000webhostapp.com/scandi_task/').then(responseHandler).catch(err => console.log(err))
    }
  }, [updateList])
  
  return (
    <div className={classes.ProductList}>
      {data.map((item) => {
        switch (item.type) {
          case "dvd":
            return (
              <ProductCard
                key={item.sku}
                SKU={item.sku}
                name={item.name}
                price={item.price}
                size={item.size}
              />
            );
          case "book":
            return (
              <ProductCard
                key={item.sku}
                SKU={item.sku}
                name={item.name}
                price={item.price}
                weight={item.weight}
              />
            );
          case "furniture":
            return (
              <ProductCard
                key={item.sku}
                SKU={item.sku}
                name={item.name}
                price={item.price}
                length={item.length}
                width={item.width}
                height={item.height}
              />
            );

          default:
            break;
        }
      })}
    </div>
  );
};

export default ProductList;

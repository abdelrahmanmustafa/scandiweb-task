import React, { useEffect ,useState, useRef} from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import classes from "./ProductForm.module.css";
import { useSelector,useDispatch } from "react-redux";
import { setSaveProduct } from "../../features/productSlice";
import { setUpdateList } from "../../features/updateSlice";


import Switcher from "../../components/Switcher/Switcher";
const ProductForm = (props) => {
    const { saveProduct } = useSelector((state) => state);
    const skuRef = useRef();
    const nameRef = useRef();
    const priceRef = useRef();
  
    const [switcherData, setSwitcherData] = useState({});
    const [saveData, setSaveData] = useState(false);
    const [Redirect, setRedirect] = useState(false);

    let navigate = useNavigate();
    
    const dispatch = useDispatch();

    const getSwitcherData = (data)=>{
      setSwitcherData({...data}) 
      setSaveData(true);
      } 

    useEffect(() => {
        if (saveData) { 
          const sku =skuRef.current.value;
          const name = nameRef.current.value;
          const price = priceRef.current.value;
          
          if (sku.trim().length ===0){

          }
          else if(name.trim().length ===0){

          }
          else if(price.trim().length ===0 && !price.isNumber()){
            console.log('d')
          }else{
          const data = {
            sku,
            name,
            price,
            ...switcherData,
          };
          setSaveData(false);
          dispatch(setSaveProduct(false))


          fetch('https://my-scandi-task.000webhostapp.com/scandi_task/', {
            method: 'post',
            
            body: JSON.stringify(data),
          }).then(function(response) {
           
              setRedirect(true);
              return response.json();
         
          }); 

      
      
      }

    }
        
      },  [saveData]);

      useEffect(() => {
        if(Redirect == true){
          dispatch(setUpdateList(true));
        return navigate("/");
        }
      }, [Redirect])


  return (
    <div id = "product_form" className={classes.ProductForm}>

        <form className={classes.DetailForm}  >
      <label>SKU:
        <input id = "sku"
          type="text"   ref={skuRef}
        />
      </label>
      <label>Name:
        <input  id = "name"
        ref={nameRef}
        />
      </label>
      <label>Price:
        <input  id = "price"
          type="text" 
          ref={priceRef}
        />
       </label> 
       <Switcher getSwitcherData = {getSwitcherData}/>


    </form>
    </div>
  );
};

export default ProductForm;
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ProductListPage from "./pages/ProductListPage/ProductListPage";
import AddProductPage from "./pages/AddProductPage/AddProductPage";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<ProductListPage />} />
          <Route path="/add-product" element={<AddProductPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
